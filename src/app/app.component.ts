import { Component, OnDestroy } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppState } from './store/index';
import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';
import { takeUntil, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import * as fromStore from './store';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnDestroy {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }
  ];
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private store: Store<AppState>,
    private router: Router,
  ) {
    this.initializeApp();
    this.store.pipe(
      select(fromStore.authStatus),
      takeUntil(this.destroy$),
      tap((authenticated) => {
        if (authenticated) {
          this.router.navigateByUrl('home');
        } else {
          this.router.navigateByUrl('login');
        }
      })
      ).subscribe();
  }

  logout(): void {
    this.store.dispatch( { type: '[Auth] Logout' });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
