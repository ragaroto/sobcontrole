import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    authState: any = null;

    constructor(private af: AngularFireAuth,
                private router: Router) {
    }

    // Returns true if user is logged in
    get authenticated(): boolean {
        return this.authState !== null;
    }

    // Returns current user
    get currentUser(): any {
        return this.authenticated ? this.authState.auth : null;
    }

    // Returns current user UID
    get currentUserId(): string {
        return this.authenticated ? this.authState.uid : '';
    }

    currentUserAsObservalbe(): Observable<firebase.User> {
        return this.af.authState;
    }

    public loginGoogle(): Observable<auth.UserCredential> {
        return new Observable( obs => {
            this.af.auth.signInWithPopup(new auth.GoogleAuthProvider())
                        .then( suc => {
                            obs.next(suc);
                            obs.complete();
                        })
                        .catch(err =>  obs.error(err) );
        });
    }
    public loginFacebook(): Promise<auth.UserCredential> {
        return this.af.auth.signInWithPopup(new auth.FacebookAuthProvider());
    }

    //// Sign Out ////
    signOut(): Observable<boolean> {
        return new Observable(observer => {
            this.af.auth.signOut()
                .then( suc => { observer.next(); observer.complete(); })
                .catch(err => observer.error(err) );
            });
    }


    //// Helpers ////

    // private updateUserData(): void {
    // // Writes user name and email to realtime db
    // // useful if your app displays information about users or for admin features

    //   let path = `users/${this.currentUserId}`; // Endpoint on firebase
    //   let data = {
    //                name: this.currentUser.displayName,
    //                email: this.currentUser.email,
    //              }

    //   this.db.object(path).update(data)
    //   .catch(error => console.log(error));

    // }
}
