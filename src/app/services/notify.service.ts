import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class NotifyService {
    
    constructor(public alertController: AlertController) {}

    async presentAlert(
        shortParams?: Array<any>,
        longParams?: { header: string, subHeader: string, message: string, buttons: string[]} ) {
        const alert = await this.alertController.create({
          header: shortParams ? shortParams[0] : (longParams  ? longParams.header : 'Alert' ) ,
          subHeader: shortParams ? shortParams[1] : (longParams  ? longParams.subHeader : 'SubTitle' ) ,
          message: shortParams ? shortParams[2] : (longParams  ? longParams.message : 'Message' ),
          buttons: shortParams ? shortParams[3] : (longParams  ? longParams.buttons : ['Ok'] ) ,
        });
        await alert.present();
      }
}

