import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from './../store';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage  {

  constructor(private store: Store<fromStore.AppState>) { }

  loginGoogle() {
    this.store.dispatch({type: '[Auth] Login Google'});
  }


}
