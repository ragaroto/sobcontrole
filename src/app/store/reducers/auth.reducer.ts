import { Action, createReducer, on } from '@ngrx/store';
import * as AuthActions from './../actions/auth.action';

export interface AuthState {
    authenticated: boolean;
    loading: boolean;
    loaded: boolean;
    error: boolean;
    uid: string;
    credential: firebase.auth.AuthCredential;
    profile: firebase.auth.AdditionalUserInfo;
}

export const initialAuthState = {
    authenticated: false,
    loading: false,
    loaded: false,
    error: false,
    uid: null,
    credential: null,
    profile: null,
};

const authStateReducer = createReducer(
    initialAuthState,
    on(AuthActions.loginEmail,    state => ({ ...initialAuthState, ...state, loading: true, loaded: false, error: false })),

    on(AuthActions.loginFacebook, state => ({ ...initialAuthState, ...state, loading: true, loaded: false, error: false })),

    on(AuthActions.loginGoogle,   state => ({ ...initialAuthState, ...state, loading: true, loaded: false, error: false })),

    on(AuthActions.loginSucessul, (state, action) => {
        return ({ ...state, ...action.payload, loading: false, loaded: true, error: false, authenticated: true });
    }),

    on(AuthActions.loginFailed,   state => ({ ...state, ...state, loading: false, loaded: true, error: true })),

    on(AuthActions.logout,           (x) => {
        return ({ ...x, loading: true, loaded: false, error: false });
    }),

    on(AuthActions.logoutSuccessful, (x) => {
        return ({ ...x, loading: false, loaded: true, error: false, authenticated: false });
    }),
    on(AuthActions.logoutFailed,     (x) => ({ ...x, loading: false, loaded: true, error: true })),
);

export function authReducer(state: AuthState | undefined, action: Action) {
    return authStateReducer(state, action);
}



