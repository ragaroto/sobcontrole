export * from './actions/auth.action';
export * from './reducers/auth.reducer';
export * from './effects/auth.effect';

import { AuthState } from './reducers/auth.reducer';
import { createSelector } from '@ngrx/store';
import { Observable } from 'rxjs';

export interface AppState {
    auth: AuthState;
}

/*
    Selectors
*/

export const selectAuthState = (state: AppState) => state.auth;

export const authStatus = createSelector(
    selectAuthState,
    (state: AuthState) => state.authenticated
);


