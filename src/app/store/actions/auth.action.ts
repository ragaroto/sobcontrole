import { createAction, props } from '@ngrx/store';

export const loginEmail     = createAction(
    '[Auth] Login Email',
     props<{ username: string; password: string }>()
);

export const loginFacebook      = createAction(
    '[Auth] Login Facebook'
);

export const loginGoogle        = createAction(
    '[Auth] Login Google'
);

export const logout             = createAction(
    '[Auth] Logout'
);

export const logoutSuccessful   = createAction(
    '[Auth] Logout Successful',
);

export const logoutFailed       = createAction(
    '[Auth] Logout Failed',
);

export const loginSucessul      = createAction(
    '[Auth] Login Successful',
    props<{payload: {
        uid: string,
        credential: firebase.auth.AuthCredential,
        profile: firebase.auth.AdditionalUserInfo
    }} >()
);

export const loginFailed        = createAction(
    '[Auth] Login Failed',
    props<{}>()
);




