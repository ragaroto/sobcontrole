import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, catchError, exhaustMap, tap } from 'rxjs/operators';
import { AuthService } from './../../services/auth.service';
import { NotifyService } from './../../services/notify.service';


@Injectable()
export class AuthEffects {

    login$ = createEffect(() => this.actions$.pipe(
        ofType('[Auth] Login Google'),
        exhaustMap(() => {
            return this.authService.loginGoogle().pipe(
                map((user) => {
                    return ({ type: '[Auth] Login Successful', payload: {
                        uid: user.user.uid,
                        profile: user.additionalUserInfo.profile,
                        credential: user.credential,
                    } });
                }),
                catchError(err => {
                    this.notifyService.presentAlert(['Error', 'Login error', 'Something went worng', ['Ok']]);
                    return of({type: '[Auth] Login Failed'});
                })
            );
        })
    )
    );

    logout$ = createEffect(() => this.actions$.pipe(
        ofType('[Auth] Logout'),
        exhaustMap(() => {
            return this.authService.signOut().pipe(
                map(() => {
                    return ({ type: '[Auth] Logout Successful'} );
                }),
                catchError((x) => {
                    this.notifyService.presentAlert(['Error', 'Logout error', 'Something went worng', ['Ok']]);
                    return of( {type: '[Auth] Logout Failed'});
                })
            );
        })
        )
    );

    constructor(
        private actions$: Actions,
        private authService: AuthService,
        private notifyService: NotifyService
    ) { }
}



