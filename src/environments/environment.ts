// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB5U5dAOlDrYjd5sWQb-9VkPP91HcRnF6A',
    authDomain: 'sobcontrole-ed549.firebaseapp.com',
    databaseURL: 'https://sobcontrole-ed549.firebaseio.com',
    projectId: 'sobcontrole-ed549',
    storageBucket: 'sobcontrole-ed549.appspot.com',
    messagingSenderId: '181729122926',
    appId: '1:181729122926:web:057784229c049e82f4a47f',
    measurementId: 'G-H53RVMXZFM'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
